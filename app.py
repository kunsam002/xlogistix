#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from flask import Flask, render_template, request, make_response
from flask.ext.mail import Mail, Message
from flask.ext.script import Manager, Shell
from datetime import datetime
import json


app = Flask(__name__)
# mail = Mail(app)
manager = Manager(app)

logger = app.logger

app.config.update(
    DEBUG = True,
    MAIL_SERVER = "smtp.mail.yahoo.com",
    MAIL_PORT = 587,
    MAIL_USE_TLS = True,
    MAIL_USE_SSL = False,
    MAIL_USERNAME = "kunsam002@yahoo.com",
    MAIL_PASSWORD = "per7ectionset",
    # DEFAULT_MAIL_SENDER = 'kunsam002@yahoo.com'
)

mail = Mail(app)

def make_shell_context():
    return dict(app=app)
manager.add_command("shell", Shell(make_context=make_shell_context))


def email_sender(recipients, success_msg, **kwargs):
	msg = Message("CONTACT MESSAGE: %s"%kwargs.get("subject",""), sender=kwargs.get("email",""), recipients=recipients)
	msg.body="Sender:"+kwargs.get("name","")+"\n Message:"+kwargs.get("message","")
	msg.html="<p><strong>Sender:</strong>"+kwargs.get("name","")+"</p>"+"<p><strong>Message:</strong>"+kwargs.get("message","")+"</p>"
	
	data = {}

	try:
		mail.send(msg)
		kwargs["messageSuccess"] = success_msg
		kwargs["success"] =True
		data = json.dumps(kwargs)
		response = make_response(data, 200)
		return response

	except:
		data = json.dumps({"errors":kwargs,"success":False})
		response = make_response(data, 200)
		return response
	

def quote_requester(recipients, success_msg, **kwargs):
	msg = Message("QUOTE REQUEST: %s"%kwargs.get("subject",""), sender=kwargs.get("email",""), recipients=recipients)
	msg.body="Sender:"+kwargs.get("name","")+"\n Message:"+kwargs.get("message","")
	msg.html="<p><strong>Sender:</strong>"+kwargs.get("name","")+"</p>"+"<p><strong>City:</strong>"+kwargs.get("city","")+"</p>"+"<p><strong>Service:</strong>"+kwargs.get("service","")+"</p>"+"<p><strong>Message:</strong>"+kwargs.get("message","")+"</p>"
	
	data = {}

	try:
		mail.send(msg)
		kwargs["messageSuccess"] = success_msg
		kwargs["success"] =True
		data = json.dumps(kwargs)
		response = make_response(data, 200)
		return response

	except:
		data = json.dumps({"errors":kwargs,"success":False})
		response = make_response(data, 200)
		return response


@app.errorhandler(404)
def page_not_found(e):

	page_title= "404- Page Not Found"
	error_code= "404"
	error_title= "Sorry Page not found!"
	error_info= "The requested page cannot be found or does not exist. Please contact the Administrator."

	return render_template('error.html', **locals()), 404


@app.errorhandler(500)
def internal_server_error(e):

	page_title= "500- Internal Server Error"
	error_code= "500"
	error_title= "Sorry there has been a Server Error!"
	error_info= "There has been an Internal server Error. Please try again later or Contact the Administrator."

	return render_template('main/error.html', **locals()), 500


@app.context_processor
def main_context():
	""" Include some basic assets in the startup page """
	today = datetime.today()
	current_year = today.strftime('%Y')

	return locals()


@app.route('/')
def index():
	page_title="Home"
	wrapper_class="homepage homepage-1"
	return render_template('index.html', **locals())


@app.route('/contact-us/', methods=['GET','POST'])
def contact_us():
	page_title="Contact Us"
	wrapper_class="homepage common-page contact-us-page"
	ang_module="contactApp"

	if request.method == "POST":
		_raw_data = request.form
		raw_data= dict(_raw_data)
		data = {}
		if raw_data != {}:
			data={"message":str(raw_data["message"][0]),"subject":str(raw_data["subject"][0]),"email":str(raw_data["email"][0]),"name":str(raw_data["name"][0])}

		recipients = ['info@xlogistix.co.uk']
		msg = "Your Message was sent successfully."
		res = email_sender(recipients, msg, **data)

		return res

	return render_template('contact-us.html', **locals())


@app.route('/about-us/', methods=['GET','POST'])
def about_us():
	page_title="About Us"
	wrapper_class="homepage common-page about-us-page about"
	return render_template('about-us.html', **locals())


@app.route('/services/', methods=['GET','POST'])
def services():
	page_title="Services"
	wrapper_class="homepage common-page service-page"
	return render_template('services.html', **locals())


@app.route('/locations/', methods=['GET','POST'])
def locations():
	page_title="Locations"
	wrapper_class="homepage common-page location-page"
	return render_template('locations.html', **locals())


@app.route('/request-quote/', methods=['GET','POST'])
def request_quote():
	page_title="Request A Quote"
	wrapper_class="homepage common-page request-page"
	ang_module="requestApp"

	if request.method == "POST":
		raw_data = request.form
		raw_data= dict(raw_data)

		data={"message":str(raw_data["message"][0]),"city":str(raw_data["city"][0]),"subject":str(raw_data["subject"][0]),"email":str(raw_data["email"][0]),"service":str(raw_data["service"][0]),"name":str(raw_data["name"][0])}

		recipients = ['info@xlogistix.co.uk']
		msg = "Your Quote Request was sent Successfully."
		res = quote_requester(recipients, msg, **data)

		return res

	return render_template('request-quote.html', **locals())



# if __name__ == '__main__':
#     app.run(debug=True)


if __name__ == '__main__':
    manager.run()